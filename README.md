# Ejemplos de la prácticas de IPC

## Objetivo

Aplicación creada para desarrollar las primeras app que nos servirá de playground para aprender las bases de Flutter.

Tener ejemplos prácticos y explicativos de los conceptos de flutter.

Recordar que la documentación oficial sobre los widgets esta en <https://api.flutter.dev/flutter/widgets/widgets-library.html>

## Como se ha creado este proyecto

- Abrir con el VSCode
- Ir a "Ver/Paleta de Comandos"(View/Command Pallete) o pulsar Ctrl+Mayusculas+P  
- Poner/Seleccionar la opción "Flutter: New Project"
  - Seleccionar la opción "Application"  
    - Seleccionar el directorio destino,  
      - recomendación que el direcorio sea /src/[namespace de la applicación]
    - Poner el nombre de la aplicación
      - en este caso se puso en nombre de "ejemplos_practicas"

A partir de ahí ya se ha ido haciendo las modificaciones oportunas.

## Estructura del Proyecto de flutter

### Directorios  

#### android / ios / linux / macos / web / windows

Contienen el código del proyecto nativo para los distintos tipos de aplicaciones.

El código de Flutter se compila e inyecta en ambos proyectos para que podamos ejecutarlos (e.g. en el emulador).

Los distintos directorios pueden aparecer según como se creara el proyecto inicial (y las opciones de desarrollo que se tuviera).

#### build

Contiene el código compilado del proyecto, de forma automática, por lo que no se debe ‘tocar’

#### lib

El directorio lib/ es donde se encuentran los ficheros .dart que conforman nuestra aplicación, donde se debe de programar con dart.

### Archivos

#### pubspec.yaml

Es el fichero de configuración del proyecto:

- Versión y nombre del proyecto  
- Añadir/actualizar/eliminar dependencias
- Definir donde se encuentran los assets de la app (fuentes y material multimedia)

Mucho ojo con las identación del archivo.

Para más información, mira <https://dart.dev/tools/pub/pubspec>

##### Sección dependences

Para añadir nuevos paquetes que se pueden usar, puedes ejecutar desde el terminal:

- flutter pub add [nombre del paquete]
  - Información relacionada:
    - Paquetes con dart <https://dart.dev/tools/pub/cmd/pub-add>
    - Paquetes con Flutter [ES] <https://esflutter.dev/docs/development/packages-and-plugins/using-packages> o [EN] <https://docs.flutter.dev/development/packages-and-plugins/using-packages>
    - Buscador de paquetes <https://pub.dev/>

Para descargar los paquetes, se puede utiliza la aplicación VSCode:

- Ir a "Ver/Paleta de Comandos"(View/Command Pallete) o pulsar Ctrl+Mayusculas+P  
  - Poner/Seleccionar la opción "Flutter: Get Packages"

Para actualizar los paquetes que utiliza la aplicación:

- Ir a "Ver/Paleta de Comandos"(View/Command Pallete) o pulsar Ctrl+Mayusculas+P  
  - Poner/Seleccionar la opción "Flutter: Upgrate Packages"

##### Sección flutter.assets

- <https://flutter.dev/assets-and-images/#from-packages>
- <https://flutter.dev/custom-fonts/#from-packages>
- <https://flutter.dev/assets-and-images/#resolution-aware>

#### lib/main.dart

Contiene la función main(), el punto de entrada a la aplicación.

## Herramientas al debbuguear

### Hot reload

Actualiza la interfaz sin necesidad de cargar todo el código desde 0.
Las variables NO se reinicializan.

### Hot restart

Similar a reiniciar la app, aunque bastante más rápido.
Las variables se resetean.

### Flutter Inspector

Nor permite ‘pinchar’ en un widget y:

- acceder al código (útil en interfaces complejas).
- visualizar la distribución de los widgets en el espacio de la aplicación.

Para más información, mira <https://docs.flutter.dev/development/tools/devtools/inspector>

## Nota

Para poder previsualizar los markdown desde VSCode pulsa Ctrl+K V .

Para más información, mira <https://code.visualstudio.com/Docs/languages/markdown>
