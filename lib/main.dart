//Los imports en Dart, por defecto, importan todo el contenido en el fichero actual; sin embargo, el espacio de nombres del fichero puede quedar muy "sucio" , y en ocasiones puede hablar colisión de nombres.
//Por ello, si utilizamos la extensión as name, solo accedemos al contenido importado a través del nombre.
import 'dart:developer' as dev;
//Otra alternativa en caso que queramos acceder únicamente a algunos de los nombres, es utilizar show para elegir que nombres queremos añadir al espacio de nombres actual:
//import 'dart:developer' show log;

//P1: Paso 2=> Importación del paquete de material
//Entorno visual tipo Material Designer (Google)
import 'package:flutter/material.dart';
//Otra opción de entorno visual, tipo iOS
//import 'package:flutter/cupertino.dart';

//importación de un paquete propio del proyecto:
import 'examples/samples_controller.dart';
// otra forma de poner la importación con ruta absoluta:
// import 'package:ipc/examples/samples_controller.dart';
// el paquete se llama ipc puesto que está definido en el archivo pubspec.yaml en la propiedad name.

import 'package:flutter_localizations/flutter_localizations.dart';

//P1: Paso 1 => Definición del la función main
void main() {
  dev.log("Punto de entrada de la aplicación.");
  //P1: Paso 3 => Se llama a runApp() dentro del main()
  //              Esta función acepta como parámetro... un Widget!
  //              Para crear ese Widget, se creará una clase que herede de StatelessWidget (clase abstacta)
  runApp(const MyApp());
}

//P1: Paso 4 =>  Clase que hereda de StatelessWidget (que a su vez hereda de Widget)
//
// Esta clase es la principal de la aplicación, pues es llamada desde el main.
// Todo en Flutter son Widgets (se puede ver claramente con el Widget Inspector)
// La idea es montar nuestras aplicaciones en base a conjuntos de Widgets
//     e.g. botones, imágenes, barras de progreso, contenedores, textos...
// Modularizar la interfaz

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  //P1: Paso 5 => Sobreescribir el método build() dentro de esta clase, que es el que define la interfaz.
  // Este widget es el principal de la aplicación.
  // Todo en Flutter son Widgets
  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    // MaterialApp es un Widget que nos permite utilizar Material Design en nuestras aplicaciones. Permite definir la configuración global de la app
    // Se está invocando a su constructor () con parámetros con nombre:
    //   title: 	Identificador interno de la app
    //   home:		El Widget/Página que se mostrará al ejecutarla
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: const [
        Locale('es', 'ES'), //Español de españa
        Locale('en'), //Inglés, cualquier país
      ],
      theme: ThemeData(
          // Esto es el tema principal de la aplicación.
          //
          // Puedes arrancar la aplicación con "flutter run" o si estas con VSCode
          // con el icono que tienes disponible en esta misma ventana en la parte superior derecha (Start Debugging)
          primarySwatch: Colors.blue),
      title: 'Ejemplos',
      home:
          //TODO: cambia el parámetro de la función según el ejemplo que quieras vez.
          //Cuando modificamos un Widget, Hot Reload recarga SOLO ese Widget (actualiza la interfaz sin necesidad de cargar todo el código desde 0 sin reiniciar las variables), por eso es tan rápido (detecta que su estado ha cambiado)
          StateWidgetController.getWidget(ExampleEnum.p02e01Network),
    );
  }
}
