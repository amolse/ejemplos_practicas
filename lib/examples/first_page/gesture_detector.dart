import 'package:flutter/material.dart';
import 'container.dart';

class MyGestureDetector extends StatelessWidget {
  const MyGestureDetector({super.key});
//Cuando envolvemos un Widget con un GestureDetector, podemos detectar gestos que el usuario haga sobre él
// Todos los atributos relacionados con gestor, son funciones que se ejecutarán cuando tal evento ocurra

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onDoubleTap: () => debugPrint(
            "Capturado el onDoubleTap del Container => Has hecho doble click sobre el Container"),
        onDoubleTapCancel: () => debugPrint(
            "Capturado el onDoubleTapCancel del Container => Has cancelado el doble click sobre el Container"),
        onDoubleTapDown: (details) =>
            debugPrint("Capturado el onDoubleTapDown ($details) del Container"),
        onForcePressEnd: (details) =>
            debugPrint("Capturado el onForcePressEnd ($details) del Container"),
        onForcePressPeak: (details) => debugPrint(
            "Capturado el onForcePressPeak ($details) del Container"),
        onForcePressStart: (details) => debugPrint(
            "Capturado el onForcePressStart ($details) del Container"),
        onHorizontalDragCancel: () =>
            debugPrint("Capturado el onHorizontalDragCancel del Container"),
        onLongPress: () => debugPrint("Capturado el onLongPress del Container"),
        onLongPressCancel: () =>
            debugPrint("Capturado el onLongPressCancel del Container"),
        onLongPressUp: () =>
            debugPrint("Capturado el onLongPressUp del Container"),
        onPanCancel: () => debugPrint("Capturado el onPanCancel del Container"),
        onSecondaryLongPress: () =>
            debugPrint("Capturado el onSecondaryLongPress del Container"),
        onSecondaryLongPressCancel: () =>
            debugPrint("Capturado el onSecondaryLongPressCancel del Container"),
        onSecondaryLongPressUp: () =>
            debugPrint("Capturado el onSecondaryLongPressUp del Container"),
        onSecondaryTap: () =>
            debugPrint("Capturado el onSecondaryTap del Container"),
        onSecondaryTapCancel: () =>
            debugPrint("Capturado el onSecondaryTapCancel del Container"),
        onTap: () => debugPrint(
            "Capturado el onTap del Container => Has pulsado el Container"),
        onTapCancel: () => debugPrint("Capturado el onTapCancel del Container"),
        onTertiaryLongPress: () =>
            debugPrint("Capturado el onTertiaryLongPress del Container"),
        onTertiaryLongPressCancel: () =>
            debugPrint("Capturado el onTertiaryLongPressCancel del Container"),
        onTertiaryLongPressUp: () =>
            debugPrint("Capturado el onTertiaryLongPressUp del Container"),
        onTertiaryTapCancel: () =>
            debugPrint("Capturado el onTertiaryTapCancel del Container"),
        //Este Widget permite que los Widgets no aparezcan debajo de elementos como barras de estado o el notch.
        child:
            //const MyContainerOrangeWithDecoration());
            const MyContainerOrange());
  }
}
