import 'package:flutter/material.dart';
import 'container.dart';

class MyCenter extends StatelessWidget {
  const MyCenter({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
        //Este Widget permite que los Widgets no aparezcan debajo de elementos como barras de estado o el notch.
        child:
            //MyContainerOrangeWithDecoration());
            //MyContainerOrange());
            MyContainerRed());
  }
}
