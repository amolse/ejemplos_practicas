import 'package:flutter/material.dart';

class MyRow extends StatelessWidget {
  const MyRow({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Row(
      //Este Widget permiten distribuir, en forma de horizontalmente sus Widgets ‘hijos’
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 20,
          height: 20,
          color: Colors.redAccent,
        ),
        Container(
          width: 40,
          height: 20,
          color: Colors.blue,
        ),
        Container(
          width: 60,
          height: 20,
          color: Colors.orangeAccent,
        ),
        Container(
          width: 80,
          height: 20,
          color: Colors.lightGreen,
        ),
      ],
    ));
  }
}

class MyColumn extends StatelessWidget {
  const MyColumn({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
            width: 230,
            color: Colors.black45,
            child: Column(
              //Este Widget permiten distribuir, en forma de verticalmente sus Widgets ‘hijos’
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //mainAxisAlignment: MainAxisAlignment.center,
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  width: 20,
                  height: 20,
                  color: Colors.redAccent,
                ),
                //https://api.flutter.dev/flutter/widgets/Spacer-class.html
                //const Spacer(),
                const Spacer(flex: 1),
                Container(
                  width: 40,
                  height: 20,
                  color: Colors.blue,
                ),
                Container(
                  width: 60,
                  height: 20,
                  color: Colors.orangeAccent,
                ),
                //const Spacer(),
                const Spacer(flex: 3),
                Container(
                  width: 80,
                  height: 20,
                  color: Colors.lightGreen,
                ),
              ],
            )));
  }
}
