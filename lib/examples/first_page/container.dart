import 'package:flutter/material.dart';

/// el Container que hemos creado ocupará toda la pantalla
/// Normalmente, toda página está embebida en un Widget de tipo Scaffold
class MyContainerRed extends StatelessWidget {
  const MyContainerRed({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      //Este Widget permite definir un espacio y una posición en la pantalla, así como añadir widgets ‘hijos’
      //Sin “hijos”, ocupa todo el espacio disponible

      color: Colors.red,
    );
  }
}

class MyContainerOrange extends StatelessWidget {
  const MyContainerOrange({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      //Este Widget permite definir un espacio y una posición en la pantalla, así como añadir widgets ‘hijos’
      //Sin Scaffold como padre lo ocuparía todo.
      width: 100,
      height: 200,
      //En ocasiones, puede ser interesante que queramos que un contenedor ocupe todo el ancho o alto disponible
      //width: double.infinity,
      //En ocasiones, puede ser interesante que queramos que un contenedor ocupe todo el ancho o alto disponible
      //height: double.infinity,
      color: Colors.deepOrange,
    );
  }
}

class MyContainerOrangeWithDecoration extends StatelessWidget {
  const MyContainerOrangeWithDecoration({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        //Este Widget permite definir un espacio y una posición en la pantalla, así como añadir widgets ‘hijos’
        //Sin Scaffold como padre lo ocuparía todo.
        width: 100,
        height: 200,
        decoration: BoxDecoration(
            color: Colors.deepOrange,
            //puedes descomentar la propiedad shape y comentar las propiedades de los borders
            //shape: BoxShape.circle
            borderRadius: BorderRadius.circular(40),
            border: Border.all(color: Colors.blue, width: 8)));
  }
}
