import 'package:flutter/material.dart';
import 'container.dart';

class MySafeArea extends StatelessWidget {
  const MySafeArea({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
        //Este Widget permite que los Widgets no aparezcan debajo de elementos como barras de estado o el notch.
        child: //MyContainerOrangeWithDecoration()
            MyContainerOrange());
  }
}
