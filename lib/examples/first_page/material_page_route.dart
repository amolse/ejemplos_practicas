import 'package:flutter/material.dart';
import 'container.dart';

class MyFirstPage extends StatelessWidget {
  const MyFirstPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("First page"),
      ),
      body: GestureDetector(
          onTap: () {
            //Este Widget permite realizar una navegación básica entre pantallas/páginas/ventana, con una animación
            //SecondPage añadimos una appBar, la navegación hacia atrás se maneja de forma automática
            //Si no, podemos llamar al método pop() del objeto global Navigator de manera manual
            var route =
                MaterialPageRoute(builder: (context) => const MySecondPage());
            //https://api.flutter.dev/flutter/widgets/Navigator-class.html
            Navigator.of(context).push(route);
          },
          child: //MyContainerOrangeWithDecoration()
              const MyContainerOrange()),
    );
  }
}

class MySecondPage extends StatelessWidget {
  const MySecondPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Second page"),
        ),
        body: const MyContainerRed());
  }
}
