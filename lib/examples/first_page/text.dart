import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  const MyText({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Center(
            child: Container(
                width: 200,
                height: 100,
                color: const Color(0xFFAACCAD),
                child: const Text(
                    "Hola, IPC mola mogollón, porque podemos desarrollar apps para móviles, web y escritorio.",
                    overflow: TextOverflow.visible,
                    //overflow: TextOverflow.fade,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.indigo,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Times New Roman")))));
  }
}
