import 'package:flutter/material.dart';

/// Ejemplo de cuadro de texto de entrada por defecto
/// Un TextField permite al usuario introducir información en forma de texto
/// Ya sea para: Nombres, teléfonos, passwords, fechas, emails...
/// En el momento en que se pulse en el formulario, el teclado software del dispositivo móvil aparecerá
class MyTextField extends StatelessWidget {
  const MyTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [TextField(), SizedBox(), Text("Result")]),
    );
  }
}

/// Ejemplo de cuadro de texto de entrada por defecto
/// Algunos de sus campos más interesantes a la hora de llamar al constructor son:
/// - keyboardType:	Alteran el teclado según el tipo de información a introducir (passwords, email, fechas...)
/// - decoration:	Permite alterar el aspecto para mejorar el aspecto del campo de texto
/// - controller:	Nos permite establecer un objeto para recuperar la información que haya introducido el usuario ( no se ve en este ejemplo, sino en el siguiente)
class MyTextFieldWithInputDecoration extends StatelessWidget {
  const MyTextFieldWithInputDecoration({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        TextField(
          keyboardType: TextInputType.name,
          decoration: InputDecoration(
            icon: Icon(Icons.all_inclusive),
            labelText: "IPC",
            suffixIcon: Icon(Icons.vpn_key),
            hintText: "Introduce datos ...",
          ),
        ),
        SizedBox(),
        Text("Result")
      ]),
    );
  }
}

/// MyTextFieldWithInputDecoration tras realizar la acción de "Convert to StatefulWidget".
class MyTextFieldWithFailtController extends StatefulWidget {
  const MyTextFieldWithFailtController({super.key});

  @override
  State<MyTextFieldWithFailtController> createState() =>
      _MyTextFieldWithFailtControllerState();
}

class _MyTextFieldWithFailtControllerState
    extends State<MyTextFieldWithFailtController> {
  //-Inicio Paso 2
  // Declaramos un TextEditingController (late) como miembro de la clase, lo creamos en el initState() y lo liberamos en el dispose()
  late TextEditingController _controller;

  //Debemos sobreescribir estos 2 métodos
  @override

  /// El método initState() se invoca 1 vez, cuando se añade al widget a la pantalla
  void initState() {
    super.initState();
    _controller = TextEditingController();
    //-Inicio Paso 4
    // Podemos escuchar los cambios que se producen en el TextField a través del controlador (será necesario un hot restart para que el método initState() se vuelva a ejecutar)
    _controller.addListener(() {
      debugPrint("Cambio: ${_controller.text}");
    });
    //+Fin Paso 4
  }

  @override

  /// El método dispose() se invoca cuando se elimina el objeto del árbol de widgets
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  //+Fin Paso 2
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          //-Inicio Paso 3
          //3º Asignamos ese TextEditingController al campo controller cuando construimos TextField
          // (TextField ha dejado de se const y tambien sus widgets padres.)
          children: [
            TextField(
              controller: _controller,
              keyboardType: TextInputType.name,
              // (InputDecoration ahora debe ser const puesto que su padre no lo es y este no varia)
              decoration: const InputDecoration
                  //+Fin Paso 3
                  (
                icon: Icon(Icons.all_inclusive),
                labelText: "IPC",
                suffixIcon: Icon(Icons.vpn_key),
                hintText: "Introduce datos ...",
              ),
            ),
            const SizedBox(),
            //-Inicio Paso 5
            // Con el atributo text del controller, podemos modificar el texto que aparece por pantalla cada vez que el usuario introduce algo, sin embargo, no funcionará
            Text("Result: ${_controller.text}")
            //+Fin Paso 5
          ]),
    );
  }
}
//-Inicio Paso 6
// Para que funcione como esperamos, necesitamos que nuestro widget sea Stateful (ya lo hemos hecho!), ya que esto nos permite redibujarlo cuando detectemos que haya que hacerlo (e.g. cuando el usuario ha introducido un texto nuevo y queremos reflejar ese cambio en pantalla)
// - Para decirle a Flutter que ha de redibujar los cambios, hemos de llamar al método setState() (doc)
// - Nosotros cambiaremos el texto inferior cuando el usuario acabe de introducir la información en el TextField

/// MyTextFieldWithInputDecoration tras realizar la acción de "Convert to StatefulWidget".
class MyTextFieldWithController extends StatefulWidget {
  const MyTextFieldWithController({super.key});

  @override
  State<MyTextFieldWithController> createState() =>
      _MyTextFieldWithControllerState();
}

class _MyTextFieldWithControllerState extends State<MyTextFieldWithController> {
  //-Inicio Paso 2
  // Declaramos un TextEditingController (late) como miembro de la clase, lo creamos en el initState() y lo liberamos en el dispose()
  late TextEditingController _controller;

  //Debemos sobreescribir estos 2 métodos
  @override

  /// El método initState() se invoca 1 vez, cuando se añade al widget a la pantalla
  void initState() {
    super.initState();
    _controller = TextEditingController();
    //-Inicio Paso 4
    // Podemos escuchar los cambios que se producen en el TextField a través del controlador (será necesario un hot restart para que el método initState() se vuelva a ejecutar)
    _controller.addListener(() {
      debugPrint(
          "Cambio recogido por el controlador gracias al addListener: ${_controller.text}");
    });
    //+Fin Paso 4
  }

  @override

  /// El método dispose() se invoca cuando se elimina el objeto del árbol de widgets
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  //+Fin Paso 2
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          //-Inicio Paso 3
          //3º Asignamos ese TextEditingController al campo controller cuando construimos TextField
          // (TextField ha dejado de ser const y tambien sus widgets padres.)
          children: [
            TextField(
              controller: _controller,
              //-Inicio Paso 7
              // Llamamos al setState() => Este método acepta como parámetro una función que podemos dejar vacía
              // -> Con esto, el widget se redibujará para actualizar su visualización según el ESTADO
              onEditingComplete: () {
                setState(() {});
              },
              onChanged: (value) => debugPrint(
                  "Cambio recogido por el TextField gracias al onChanged: $value"),

              //+Fin Paso 7
              keyboardType: TextInputType.name,
              // (InputDecoration ahora debe ser const puesto que su padre no lo es y este no varia)
              decoration: const InputDecoration
                  //+Fin Paso 3
                  (
                icon: Icon(Icons.all_inclusive),
                labelText: "IPC",
                suffixIcon: Icon(Icons.vpn_key),
                hintText: "Introduce datos ...",
              ),
            ),
            const SizedBox(),
            //-Inicio Paso 5
            // Con el atributo text del controller, podemos modificar el texto que aparece por pantalla cada vez que el usuario introduce algo, sin embargo, no funcionará
            Text("Result: ${_controller.text}")
            //+Fin Paso 5
          ]),
    );
  }
}
//+Fin Paso 6

class MyTextFieldForMails extends StatefulWidget {
  const MyTextFieldForMails({super.key});

  @override
  State<MyTextFieldForMails> createState() => _MyTextFieldForMailsState();
}

class _MyTextFieldForMailsState extends State<MyTextFieldForMails> {
  //-Inicio Paso 2
  // Declaramos un TextEditingController (late) como miembro de la clase, lo creamos en el initState() y lo liberamos en el dispose()
  late TextEditingController _controller;
  late bool _error;

  //Debemos sobreescribir estos 2 métodos
  @override

  /// El método initState() se invoca 1 vez, cuando se añade al widget a la pantalla
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _error = false;
    //-Inicio Paso 4
    // Podemos escuchar los cambios que se producen en el TextField a través del controlador (será necesario un hot restart para que el método initState() se vuelva a ejecutar)
    _controller.addListener(() {
      debugPrint(
          "Cambio recogido por el controlador gracias al addListener: ${_controller.text}");
    });
    //+Fin Paso 4
  }

  @override

  /// El método dispose() se invoca cuando se elimina el objeto del árbol de widgets
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  //+Fin Paso 2
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          //-Inicio Paso 3
          //3º Asignamos ese TextEditingController al campo controller cuando construimos TextField
          // (TextField ha dejado de ser const y tambien sus widgets padres.)
          children: [
            TextField(
              controller: _controller,
              //-Inicio Paso 7
              // Llamamos al setState() => Este método acepta como parámetro una función que podemos dejar vacía
              // -> Con esto, el widget se redibujará para actualizar su visualización según el ESTADO
              onEditingComplete: () {
                _error = validateEmail(_controller.text);
                setState(() {});
              },
              onChanged: (value) => debugPrint(
                  "Cambio recogido por el TextField gracias al onChanged: $value"),

              //+Fin Paso 7
              keyboardType: TextInputType.emailAddress,
              // (InputDecoration ahora debe ser const puesto que su padre no lo es y este no varia)
              decoration: !_error
                  ? const InputDecoration
                      //+Fin Paso 3
                      (
                      icon: Icon(Icons.abc),
                      labelText: "Email",
                      suffixIcon: Icon(Icons.email),
                      hintText: "Introduce el mail ...",
                    )
                  : const InputDecoration
                      //+Fin Paso 3
                      (
                      icon: Icon(Icons.error),
                      labelText: "Email",
                      suffixIcon: Icon(Icons.email),
                      hintText: "Introduce el mail ...",
                    ),
            ),
            const SizedBox(),
            //-Inicio Paso 5
            // Con el atributo text del controller, podemos modificar el texto que aparece por pantalla cada vez que el usuario introduce algo, sin embargo, no funcionará
            Text("Result: ${_controller.text}")
            //+Fin Paso 5
          ]),
    );
  }

  bool validateEmail(String? value) {
    const pattern = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'"
        r'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-'
        r'\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*'
        r'[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4]'
        r'[0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9]'
        r'[0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\'
        r'x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])';
    final regex = RegExp(pattern);

    return value!.isNotEmpty && !regex.hasMatch(value);
  }
}
//+Fin Paso 6

class MyDatePicker extends StatelessWidget {
  const MyDatePicker({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton.icon(
        icon: const Icon(Icons.favorite),
        label: const Text("Calendar time!!"),
        style: ButtonStyle(
            backgroundColor: const MaterialStatePropertyAll(Colors.pink),
            padding: const MaterialStatePropertyAll(EdgeInsets.all(30)),
            shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50))) // foreground
            ),
        onPressed: () {
          showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(1900),
              lastDate: DateTime(2099));
        },
      ),
    );
  }
}

class MyDatePickerAsync extends StatelessWidget {
  const MyDatePickerAsync({super.key});

  void selectDate(BuildContext context) async {
    final value = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime(2099));
    if (value != null) {
      debugPrint("OK => ${value.weekday}");
    } else {
      debugPrint("Cancelled!!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton.icon(
        icon: const Icon(Icons.favorite),
        label: const Text("Calendar time!!"),
        style: ButtonStyle(
            backgroundColor: const MaterialStatePropertyAll(Colors.pink),
            padding: const MaterialStatePropertyAll(EdgeInsets.all(30)),
            shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50))) // foreground
            ),
        onPressed: () => selectDate(context),
      ),
    );
  }
}

class MyDatePickerWithThen extends StatefulWidget {
  const MyDatePickerWithThen({super.key});

  @override
  State<MyDatePickerWithThen> createState() => _MyDatePickerStateWithThen();
}

class _MyDatePickerStateWithThen extends State<MyDatePickerWithThen> {
  DateTime? _selectedDate;
  String? _dateString;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Colors.red, // foreground
          ),
          child: const Text("Seleccionar fecha"),
          onPressed: () {
            showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(1900),
                    lastDate: DateTime(2099))
                .then((value) {
              _selectedDate = value;
              if (_selectedDate == null) {
                _dateString = "No value inserted";
              } else {
                _dateString =
                    "${_selectedDate?.day}/${_selectedDate?.month}/${_selectedDate?.year}";
              }
              setState(() {});
            });
          },
        ),
        const SizedBox(
          height: 30,
        ),
        Text("Fecha seleccionada: $_dateString")
      ],
    );
  }
}

class MyDatePickerConditional extends StatefulWidget {
  const MyDatePickerConditional({super.key});

  @override
  State<MyDatePickerConditional> createState() =>
      _MyDatePickerConditionalState();
}

class _MyDatePickerConditionalState extends State<MyDatePickerConditional> {
  DateTime? _birthday;

  void selectDate(BuildContext context) async {
    final value = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime(2099));
    if (value != null) {
      debugPrint("OK => ${value.weekday}");
      _birthday = value;
    } else {
      debugPrint("Cancelled!!");
      _birthday = null;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Colors.lightGreen, // foreground
          ),
          child: const Text("Seleccionar fecha"),
          onPressed: () => selectDate(context),
        ),
        const SizedBox(
          height: 30,
        ),
        if (_birthday != null)
          Text("${_birthday?.day}/${_birthday?.month}/${_birthday?.year}")
        else
          const Icon(Icons.device_unknown)
      ],
    );
  }
}

class IfElseEvenButton extends StatefulWidget {
  const IfElseEvenButton({super.key});

  @override
  State<IfElseEvenButton> createState() => _IfElseEvenButtonState();
}

class _IfElseEvenButtonState extends State<IfElseEvenButton> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            onPressed: () {
              setState(() {
                ++_counter;
              });
            },
            child: const Text("Púlsame!")),
        const SizedBox(
          height: 20,
        ),
        if (_counter.isEven)
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Es par: $_counter"),
              const SizedBox(
                height: 10,
              ),
              const Icon(Icons.catching_pokemon)
            ],
          )
        else
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Es impar: $_counter"),
              const SizedBox(
                width: 10,
              ),
              const Icon(Icons.catching_pokemon_outlined)
            ],
          )
      ],
    );
  }
}

class MyRadioGroup extends StatefulWidget {
  const MyRadioGroup({super.key});

  @override
  State<MyRadioGroup> createState() => _MyRadioGroupState();
}

class _MyRadioGroupState extends State<MyRadioGroup> {
  String? _house;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 140,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(_house == null ? "Select one:" : "You belong to $_house \n"),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Gryffindor"),
              const Spacer(),
              Radio(
                groupValue: _house,
                value: "Gryffindor",
                onChanged: (v) {
                  setState(() {
                    _house = "$v";
                  });
                },
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Slytherin"),
              const Spacer(),
              Radio(
                groupValue: _house,
                value: "Slytherin",
                onChanged: (v) {
                  setState(() {
                    _house = "$v";
                  });
                },
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Hufflepuff"),
              const Spacer(),
              Radio(
                groupValue: _house,
                value: "Hufflepuff",
                onChanged: (v) {
                  setState(() {
                    _house = "$v";
                  });
                },
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Ravenclaw"),
              const Spacer(),
              Radio(
                groupValue: _house,
                value: "Ravenclaw",
                onChanged: (v) {
                  setState(() {
                    _house = "$v";
                  });
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}

class MyModalDialog extends StatelessWidget {
  const MyModalDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      child: const Text("Abrir modal"),
      onPressed: () => showDialog(
          context: context, builder: (context) => _createAlert(context)),
    );
  }

  AlertDialog _createAlert(BuildContext context) => AlertDialog(
        title: const Text("Importante"),
        content: const Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("Lorem ipsum"),
            Text("Lorem ipsum"),
            Text("Lorem ipsum")
          ],
        ),
        actions: [
          TextButton(
              style: TextButton.styleFrom(
                foregroundColor: Colors.lightBlue,
              ),
              onPressed: () {
                debugPrint("Press Ayuda");
                Navigator.of(context).pop();
              },
              child: const Text("Ayuda")),
          TextButton(
              style: TextButton.styleFrom(
                foregroundColor: Colors.red,
              ),
              onPressed: () {
                debugPrint("Press Cancelar");
                Navigator.of(context).pop();
              },
              child: const Text("Cancelar")),
          TextButton(
              style: TextButton.styleFrom(
                foregroundColor: Colors.green,
              ),
              onPressed: () {
                debugPrint("Press Aceptar");
                Navigator.of(context).pop();
              },
              child: const Text("Aceptar")),
        ],
      );
}

class MyFooter extends StatelessWidget {
  const MyFooter({super.key});

  @override
  Widget build(BuildContext context) {
    /*return SizedBox.expand(
      child: Container(
        color: Colors.red,
        child: CustomPaint(
          painter: MyFooterPainter(),
        ),
      ),
    );
     */

    return Container(
      color: Colors.pink,
      width: double.infinity,
      height: 500,
      child: CustomPaint(
        painter: MyFooterPainter(),
      ),
    );
  }
}

class MyFooterPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint
      ..color = Colors.lightGreenAccent
      ..style = PaintingStyle.fill
      ..strokeWidth = 10.0;

    final path = Path();
    path
      ..moveTo(0, size.height - 100)
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, size.height - 100)
      ..lineTo(size.width * 0.75, size.height - 50)
      ..lineTo(size.width * 0.5, size.height - 150)
      ..lineTo(size.width * 0.25, size.height - 100);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class MyOrientatedText extends StatelessWidget {
  const MyOrientatedText({super.key});

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) {
        if (orientation == Orientation.landscape) {
          return const Text(
            "Landscape",
            style: TextStyle(fontSize: 100, color: Colors.pink),
          );
        } else {
          return const Text("Portrait",
              style: TextStyle(fontSize: 100, color: Colors.blue));
        }
      },
    );
  }
}

class MySuperResponsiveLayout extends StatelessWidget {
  final List<Widget> data;

  const MySuperResponsiveLayout({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    Widget result;

    if (width > 700.0) {
      result = SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: data,
        ),
      );
    } else {
      result = SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: data,
        ),
      );
    }

    return result;
  }
}
