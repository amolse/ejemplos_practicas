import 'package:flutter/material.dart';
import 'dart:developer' as dev;

/*
Uno de los elementos más interesantes a la hora de mostrar información en pantalla son las imágenes y los iconos
 * Una imagen es un rectángulo formado por píxeles, sin embargo, dependiendo de la resolución de la misma, frente a operaciones de escalado puede verse una pérdida de calidad.
 * Un icono vectorizado está formado por puntos, líneas y curvas, lo que permite que frente a operaciones de escalado, no pierda calidad.
 * Ejemplo vectorial en ExampleEnum.p02e18Pokemon => PokemonList()
*/

///Clase con propiedades estáticas (no necesita constructor) que nos ayuda a la saber la ubicación de los assets (utensilios como son las imagenes.)
class RutaDe {
  /// Url a una imagen.
  static const String laImagenEnUnaUrl =
      "https://media.giphy.com/media/Fx85ye9hVe2vS/source.gif";

  /// Ruta relativa a una imagen cargada como asset
  static const String laImagenComoAsset =
      "assets/examples/image/img/gradient.png";

  /// Ruta al asset que se quiere usar como imagen de ejemplo.
  static const String laImagenParaLosEjemplos = laImagenComoAsset;
}

/// Ejemplo de una Imagen cargando desde url
class MyImageNetwork extends StatelessWidget {
  ///Imagen cargando desde url
  const MyImageNetwork({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Image.network(RutaDe.laImagenEnUnaUrl);
  }
}

/// Ejemplo de una Imagen cargando desde un asset
/// Se debe de:
/// - Tener el archivo en la ruta de la variable path
/// - en el archivo pubspec.yaml poner la tura en los assets (recuerda la identación)
class MyImageAsset extends StatelessWidget {
  /// Imagen carganda desde un asset
  const MyImageAsset({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Image.asset(RutaDe.laImagenComoAsset);
  }
}

/// Ejemplo de la una Imagen sin transformaciones Centrada
class MyImageCenter extends StatelessWidget {
  /// Imagen sin transformaciones Centrada
  const MyImageCenter({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      child: Image.asset(RutaDe.laImagenParaLosEjemplos),
    );
  }
}

/// Ejemplo de la una Imagen aplicandole efectos de color (color y colorBlendMode)
class MyImageColor extends StatelessWidget {
  /// Imagen con un efectos de color rojo aplicado
  const MyImageColor({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      child: Image.asset(
        RutaDe.laImagenParaLosEjemplos,
        color: Colors.red,
        colorBlendMode: BlendMode.difference,
      ),
    );
  }
}

/// Ejemplo de una Imagen en un Container
class MyImageContainer extends StatelessWidget {
  // Imagen en un Container con un alcance máximo y sus componetes situados en la parte inferior.
  const MyImageContainer({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Container(
      alignment: Alignment.bottomCenter,
      height: double.infinity,
      width: double.infinity,
      child: Image.asset(RutaDe.laImagenParaLosEjemplos),
    );
  }
}

/// Ejemplo de una Imagen con un hueco de situación modificado. Recordar:
/// - Por defecto, una imagen ocupa el espacio según su tamaño
/// - Además, intenta mantener su relación de aspecto ancho x alto
/// - La imagen por defecto tiene un tamaño de 200x200
class MyImageSize extends StatelessWidget {
  /// Imagen centrada en un hueco de una altura de +100 y todo lo que se pueda de ancho. A pesar de ello, la imagen permanece con su tamaño.
  const MyImageSize({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      child: Image.asset(RutaDe.laImagenParaLosEjemplos,
          width: double.infinity, height: 300),
    );
  }
}

/// Ejemplo de una imagen con propiedad fit=BoxFit.none
class MyImageFitNone extends StatelessWidget {
  /// Imagen que centra en el espacio disponible, cortando los márgenes si no caben en tal espacio (mantiene la relación de aspecto)
  const MyImageFitNone({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      child: Image.asset(
        RutaDe.laImagenParaLosEjemplos,
        width: double.infinity,
        height: 300,
        fit: BoxFit.none,
      ),
    );
  }
}

/// Ejemplo de una imagen con propiedad fit=BoxFit.fitWidth
class MyImageFitWidth extends StatelessWidget {
  ///Imagen que ocupa todo el ancho disponible, sin tener en cuenta si el alto se sale (mantiene la relación de aspecto)
  const MyImageFitWidth({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      child: Image.asset(
        RutaDe.laImagenParaLosEjemplos,
        width: double.infinity,
        height: 300,
        fit: BoxFit.fitWidth,
      ),
    );
  }
}

/// Ejemplo de una imagen con propiedad fit=BoxFit.fitHeight
class MyImageFitHeight extends StatelessWidget {
  ///Imagen que ocupa todo el alto disponible, sin tener en cuenta si el ancho se sale (mantiene la relación de aspecto)
  const MyImageFitHeight({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      child: Image.asset(
        RutaDe.laImagenParaLosEjemplos,
        width: double.infinity,
        height: 300,
        fit: BoxFit.fitHeight,
      ),
    );
  }
}

/// Ejemplo de una imagen con propiedad fit=BoxFit.cover
class MyImageFitCover extends StatelessWidget {
  ///Imagen que ocupa todo el espacio disponible (mantiene la relación de aspecto)
  const MyImageFitCover({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      child: Image.asset(
        RutaDe.laImagenParaLosEjemplos,
        width: double.infinity,
        height: 300,
        fit: BoxFit.cover,
      ),
    );
  }
}

/// Ejemplo de una imagen con propiedad fit=BoxFit.contain
class MyImageFitContain extends StatelessWidget {
  ///Imagen que ocupa todo el espacio disponible, siempre que la imagen se encuentre dentro del mismo (mantiene la relación de aspecto)
  const MyImageFitContain({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      child: Image.asset(
        RutaDe.laImagenParaLosEjemplos,
        width: double.infinity,
        height: 300,
        fit: BoxFit.contain,
      ),
    );
  }
}

/// Ejemplo de una imagen con propiedad fit=BoxFit.fill
class MyImageFitFill extends StatelessWidget {
  ///Imagen que ocupa todo el espacio disponible, aunque haya que distorsionar la imagen (NO mantiene la relación de aspecto)
  const MyImageFitFill({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    return Center(
      child: Image.asset(
        RutaDe.laImagenParaLosEjemplos,
        width: double.infinity,
        height: 300,
        fit: BoxFit.fill,
      ),
    );
  }
}
