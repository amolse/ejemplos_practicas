import 'package:flutter/material.dart';

/*
Cuando un proceso que ocurre en pantalla puede durar un tiempo indeterminado (e.g. consulta web)...
-> Si la pantalla no reacciona, el usuario pensará que, o bien la app no está haciendo nada, o bien se ha quedado bloqueada
-> Debemos mostrar al usuario que hay una acción ocurriendo, mediante algún elemento visual (principio de Don Norman)
*/

/// La clase LinearProgressIndicator permite mostrar una barra de progreso por pantalla, y se puede aplicar en dos casos habituales:
/// - Carga determinada:
///   - Conocemos el % de un proceso (e.g. 0 a 100, la descarga de un fichero)
/// - Carga indeterminada
///   - No conocemos el % de un proceso, cuánto tiempo tardará (e.g. Una petición a un servicio web)
class MyLinearProgress extends StatelessWidget {
  ///Ejemplo progreso indeterminado
  const MyLinearProgress({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: const LinearProgressIndicator(),
      ),
    );
  }
}

class MyLinearProgressWithValue extends StatelessWidget {
  ///Ejemplo progreso determinado
  const MyLinearProgressWithValue({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: const LinearProgressIndicator(
          value: 0.25, // Normalizado entre 0-1
          backgroundColor: Colors.red,
        ),
      ),
    );
  }
}

class MyCircularProgressIndicator extends StatelessWidget {
  //Ejemplo progreso indeterminado
  const MyCircularProgressIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SizedBox(
        width: 200,
        height: 200,
        child: CircularProgressIndicator(
          strokeWidth: 20,
        ),
      ),
    );
  }
}

class MyCircularProgressIndicatorStopped extends StatelessWidget {
  //Ejemplo progreso indeterminado
  const MyCircularProgressIndicatorStopped({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SizedBox(
        width: 200,
        height: 200,
        child: CircularProgressIndicator(
          backgroundColor: Colors.red,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.yellow),
        ),
      ),
    );
  }
}
