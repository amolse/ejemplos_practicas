import 'package:flutter/material.dart';

/*
Sobre StatelessWidget vs. StatefulWidget
https://dev.to/flutterclutter/statelesswidget-vs-statefulwidget-5e4m
*/
class _ProgressWidgetState extends State<ProgressWidget> {
  double _progressValue = 0;
  static double increaseValue = 0.1;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          child: LinearProgressIndicator(
              value: _progressValue, backgroundColor: Colors.red)),
      //const SizedBox(height: 30),
      Text(
          "${_progressValue == 1 ? 'Completed' : (_progressValue * 100).toStringAsPrecision(3)}% (_progressValue = $_progressValue)"), // https://stackoverflow.com/questions/588004/is-floating-point-math-broken
      MaterialButton(
          color: Colors.purple,
          onPressed: (() {
            setState(() {
              if (_progressValue == 1) {
                _progressValue = 0;
                return;
              }
              _progressValue += increaseValue;
              if (_progressValue > 1) {
                _progressValue = 1;
                return;
              }
              return;
            });
          }),
          child: Text(
            _progressValue < 1
                ? "Increase progress ($_progressValue+$increaseValue)"
                : "Renew",
            style: const TextStyle(color: Colors.white),
          ))
    ]);
  }
}

///Un StatefulWidget permite tener un estado (variables) y notificar a la interfaz que, cuando el estado cambia, se ha de redibujar el widget => método setState(() {})
class ProgressWidget extends StatefulWidget {
  ///Ejemplo con StatefulWidget para poder incrementar el progreso poco a poco.
  const ProgressWidget({super.key});

  @override
  State<ProgressWidget> createState() => _ProgressWidgetState();
}
