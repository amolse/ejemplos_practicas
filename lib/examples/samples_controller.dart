import 'dart:developer' as dev;
import 'package:flutter/cupertino.dart';
import 'package:ipc/examples/between_pages/home_page.dart';
import 'package:ipc/examples/first_page/center.dart';
import 'package:ipc/examples/first_page/container.dart';
import 'package:ipc/examples/first_page/gesture_detector.dart';
import 'package:ipc/examples/first_page/material_page_route.dart';
import 'package:ipc/examples/forms/widgets.dart';
import 'package:ipc/examples/progress/widgets.dart';
import 'package:ipc/examples/future/widgets.dart';
import 'package:ipc/examples/progress/linear_progress_widget.dart';
import 'between_pages/first_page.dart';
import 'between_pages/items_page_with_hero.dart';
import 'first_page/row_and_column.dart';
import 'first_page/safearea.dart';
import 'first_page/text.dart';
import 'image/widgets.dart';
import 'listview/pokemon_list.dart';
import 'listview/widgets.dart';
import 'my_home_page.dart';

///Ejemplos implementado sobre flutter.
/// la nomenclatura es p[número de la practica con 2 digitos]e[número del ejemplo con 2 digitos][Descripción/nombre del ejemplo/widget a mostrar]
enum ExampleEnum {
  ///Widget de flutter al crear un nuevo proyecto.
  p01e00MyHome,

  ///Widget de un contenedor rojo.
  p01e01RedContainer,

  ///Widget de un Scaffold con un contenedor rojo.
  p01e02Scaffold,

  ///Widget de un contenedor naranja con alto y ancho.
  p01e03OrangeContainer,

  ///Widget de un contenedor con decoracion.
  p01e04OrangeContainerWithDecoration,

  ///Widget de SafeArea con un contenedor.
  p01e05SafeArea,

  ///Widget que centra a su hijo.
  p01e06Center,

  ///Widget con un texto centrado
  p01e07Text,

  ///Widget Row para la distribución horizontal.
  p01e08Row,

  ///Widget Row para la distribución vertical.
  p01e09Column,

  ///Widget GestureDetector para capturar eventos.
  p01e10GestureDetector,

  ///Widget MaterialPageRoute para ir de una página a otra.
  p01e11MaterialPagerRoute,

  ///Widget de una imagen con origen en una url.
  p02e01Network,

  ///Widget de una imagen con origen en un asset.
  p02e02Asset,

  ///Widget de una imagen Centrada.
  p02e03Center,

  ///Widget de una imagen manipulada el color.
  p02e04Color,
  p02e05Container,
  p02e06Size,
  p02e07FitNone,
  p02e08FitWidth,
  p02e09FitHeight,
  p02e10FitCover,
  p02e11FitContain,
  p02e12FitFill,
  p02e13List,
  p02e14Divider,
  p02e15Title,
  p02e16LazyLoading,
  p02e17Separed,
  p02e18Pokemon,
  p03e01Linear,
  p03e02LinearWithValue,
  p03e03LinearWithState,
  p03e04Circular,
  p03e05CirlularStopped,
  p03e06Future,
  p03e07GetHttpData,
  p03e08DataBetweenPages,
  p03e09Hero,
  p03e10LoadJson,
  p04e01SimpleTextField,
  p04e02TextFieldWithInputDecoration,
  p04e03TextFieldFailtController,
  p04e04TextFieldController,
  p04e05TextFieldWithValidation,
  p04e06DataPicker,
  p04e07DataPickerAsync,
  p04e08DataPickerWithThen,
  p04e09DataPickerConditional,
  p04e10WidgetConditional,
  p04e11RadioButton,
  p04e12AlertDialog,
  p04e13CustomPainter,
  p04e14Orientation,
  p04e15MediaQuery
}

/// Controlador de Widget a usar.
/// * Pasando el ExampleEnum nos permite mostrar el widget oportuno (sin scafold que lo envuelva).
class WidgetController {
  ///Devuelve el Widget relacionado con el ejemplo.
  static Widget getWidget(ExampleEnum type) {
    dev.log("Obtener el Widget para el enumerado $type .");
    switch (type) {
      case ExampleEnum.p01e01RedContainer:
      case ExampleEnum.p01e02Scaffold:
        return const MyContainerRed();
      case ExampleEnum.p01e03OrangeContainer:
        return const MyContainerOrange();
      case ExampleEnum.p01e04OrangeContainerWithDecoration:
        return const MyContainerOrangeWithDecoration();
      case ExampleEnum.p01e05SafeArea:
        return const MySafeArea();
      case ExampleEnum.p01e06Center:
        return const MyCenter();
      case ExampleEnum.p01e07Text:
        return const MyText();
      case ExampleEnum.p01e08Row:
        return const MyRow();
      case ExampleEnum.p01e09Column:
        return const MyColumn();
      case ExampleEnum.p01e10GestureDetector:
        return const MyGestureDetector();
      case ExampleEnum.p01e11MaterialPagerRoute:
        return const MyFirstPage();
      case ExampleEnum.p02e01Network:
        return const MyImageNetwork();
      case ExampleEnum.p02e02Asset:
        return const MyImageAsset();
      case ExampleEnum.p02e03Center:
        return const MyImageCenter();
      case ExampleEnum.p02e04Color:
        return const MyImageColor();
      case ExampleEnum.p02e05Container:
        return const MyImageContainer();
      case ExampleEnum.p02e06Size:
        return const MyImageSize();
      case ExampleEnum.p02e07FitNone:
        return const MyImageFitNone();
      case ExampleEnum.p02e08FitWidth:
        return const MyImageFitWidth();
      case ExampleEnum.p02e09FitHeight:
        return const MyImageFitHeight();
      case ExampleEnum.p02e10FitCover:
        return const MyImageFitCover();
      case ExampleEnum.p02e11FitContain:
        return const MyImageFitContain();
      case ExampleEnum.p02e12FitFill:
        return const MyImageFitFill();
      case ExampleEnum.p02e13List:
        return const MyList();
      case ExampleEnum.p02e14Divider:
        return const MyListWhitDivider();
      case ExampleEnum.p02e15Title:
        return const MyListTitle();
      case ExampleEnum.p02e16LazyLoading:
        return const MyListLazyLoading();
      case ExampleEnum.p02e17Separed:
        return const MyListSeparated();
      case ExampleEnum.p02e18Pokemon:
        return const PokemonList();
      case ExampleEnum.p03e01Linear:
        return const MyLinearProgress();
      case ExampleEnum.p03e02LinearWithValue:
        return const MyLinearProgressWithValue();
      case ExampleEnum.p03e03LinearWithState:
        return const ProgressWidget();
      case ExampleEnum.p03e04Circular:
        return const MyCircularProgressIndicator();
      case ExampleEnum.p03e05CirlularStopped:
        return const MyCircularProgressIndicatorStopped();
      case ExampleEnum.p03e06Future:
        return const MyFutureAfter5Seconds();
      case ExampleEnum.p03e07GetHttpData:
        return const MyFutureHttpRequest();
      case ExampleEnum.p03e10LoadJson:
        return const MyFutureLoadJson();
      case ExampleEnum.p04e01SimpleTextField:
        return const MyTextField();
      case ExampleEnum.p04e02TextFieldWithInputDecoration:
        return const MyTextFieldWithInputDecoration();
      case ExampleEnum.p04e03TextFieldFailtController:
        return const MyTextFieldWithFailtController();
      case ExampleEnum.p04e04TextFieldController:
        return const MyTextFieldWithController();
      case ExampleEnum.p04e05TextFieldWithValidation:
        return const MyTextFieldForMails();
      case ExampleEnum.p04e06DataPicker:
        return const MyDatePicker();
      case ExampleEnum.p04e07DataPickerAsync:
        return const MyDatePickerAsync();
      case ExampleEnum.p04e08DataPickerWithThen:
        return const MyDatePickerWithThen();
      case ExampleEnum.p04e09DataPickerConditional:
        return const MyDatePickerConditional();
      case ExampleEnum.p04e10WidgetConditional:
        return const IfElseEvenButton();
      case ExampleEnum.p04e11RadioButton:
        return const MyRadioGroup();
      case ExampleEnum.p04e12AlertDialog:
        return const MyModalDialog();
      case ExampleEnum.p04e13CustomPainter:
        return const MyFooter();
      case ExampleEnum.p04e14Orientation:
        return const MyOrientatedText();
      case ExampleEnum.p04e15MediaQuery:
        return const MySuperResponsiveLayout(
          data: [MyOrientatedText(), MyRadioGroup()],
        );
      default:
        throw UnimplementedError(
            "No está Implementado el Widget para el enumerado $type.");
    }
  }
}

/// Controlador de Widget a usar.
/// * Pasando el ExampleEnum nos permite mostrar el widget oportuno (añadiendo el scafold si procede).
class StateWidgetController {
  static Widget getWidget(ExampleEnum type) {
    dev.log("Obtener el widget con o sin estado para el enumerado $type .");
    switch (type) {
      case ExampleEnum.p01e00MyHome:
        return const MyHomePage(title: 'Flutter Demo Home Page');
      case ExampleEnum.p01e01RedContainer:
        return const MyContainerRed();
      case ExampleEnum.p01e02Scaffold:
      case ExampleEnum.p01e03OrangeContainer:
      case ExampleEnum.p01e04OrangeContainerWithDecoration:
      case ExampleEnum.p01e05SafeArea:
      case ExampleEnum.p01e06Center:
      case ExampleEnum.p01e07Text:
      case ExampleEnum.p01e08Row:
      case ExampleEnum.p01e09Column:
      case ExampleEnum.p01e10GestureDetector:
        return HomePage(title: "Ejemplo Practica 01", example: type);
      case ExampleEnum.p01e11MaterialPagerRoute:
        return const MyFirstPage();
      case ExampleEnum.p02e01Network:
      case ExampleEnum.p02e02Asset:
      case ExampleEnum.p02e03Center:
      case ExampleEnum.p02e04Color:
      case ExampleEnum.p02e05Container:
      case ExampleEnum.p02e06Size:
      case ExampleEnum.p02e07FitNone:
      case ExampleEnum.p02e08FitWidth:
      case ExampleEnum.p02e09FitHeight:
      case ExampleEnum.p02e10FitCover:
      case ExampleEnum.p02e11FitContain:
      case ExampleEnum.p02e12FitFill:
      case ExampleEnum.p02e13List:
      case ExampleEnum.p02e14Divider:
      case ExampleEnum.p02e15Title:
      case ExampleEnum.p02e16LazyLoading:
      case ExampleEnum.p02e17Separed:
      case ExampleEnum.p02e18Pokemon:
        return HomePage(title: "Ejemplo Practica 02", example: type);
      case ExampleEnum.p03e01Linear:
      case ExampleEnum.p03e02LinearWithValue:
      case ExampleEnum.p03e04Circular:
      case ExampleEnum.p03e05CirlularStopped:
      case ExampleEnum.p03e03LinearWithState:
      case ExampleEnum.p03e06Future:
      case ExampleEnum.p03e07GetHttpData:
      case ExampleEnum.p03e10LoadJson:
        return HomePage(title: "Ejemplo Practica 03", example: type);
      case ExampleEnum.p03e08DataBetweenPages:
        return const FirstPage();
      case ExampleEnum.p03e09Hero:
        return const ItemsPageWithHero(title: "Ejemplo de My Hero");
      case ExampleEnum.p04e01SimpleTextField:
      case ExampleEnum.p04e02TextFieldWithInputDecoration:
      case ExampleEnum.p04e03TextFieldFailtController:
      case ExampleEnum.p04e04TextFieldController:
      case ExampleEnum.p04e06DataPicker:
      case ExampleEnum.p04e07DataPickerAsync:
      case ExampleEnum.p04e08DataPickerWithThen:
      case ExampleEnum.p04e09DataPickerConditional:
      case ExampleEnum.p04e10WidgetConditional:
      case ExampleEnum.p04e11RadioButton:
      case ExampleEnum.p04e12AlertDialog:
      case ExampleEnum.p04e13CustomPainter:
      case ExampleEnum.p04e14Orientation:
      case ExampleEnum.p04e15MediaQuery:
        return HomePage(title: "Ejemplo Practica 04", example: type);
      default:
        throw UnimplementedError(
            "No está Implementado el Widget para el enumerado $type.");
    }
  }
}
