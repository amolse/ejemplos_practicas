import 'dart:developer' as dev;
import 'package:flutter/material.dart';

//imports por el ejemplo de pokemon
import 'package:ipc/examples/listview/model/pokemon_model.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PokemonList extends StatelessWidget {
  /// Ejemplo de lista alimentado sus items mediante un json
  const PokemonList({super.key});

  @override
  Widget build(BuildContext context) {
    dev.log("Renderizado de $runtimeType");
    final list = Pokemon.initial.map((pkmn) {
      Color bgColor;
      switch (pkmn.type) {
        case PokemonType.fire:
          bgColor = Colors.red.withOpacity(0.4);
          break;
        case PokemonType.plant:
          bgColor = Colors.green.withOpacity(0.4);
          break;
        case PokemonType.water:
          bgColor = Colors.lightBlue.withOpacity(0.4);
          break;
      }
      dev.log(
          "Renderizado de $runtimeType => Establecido el color $bgColor para ${pkmn.name}, ahora se define el widget para pintarlo.)");
      return Column(
        children: [
          Container(
            color: bgColor,
            child: ListTile(
              trailing: Text("Nº ${pkmn.number}"),
              title: Text(pkmn.name),
              leading: SvgPicture.asset(
                pkmn.iconPath,
                width: 30,
              ),
              onTap: () => dev.log(
                  "Presionado sobre ${pkmn.name} con el id=${pkmn.number}."),
            ),
          ),
          Divider(
            color: bgColor.withOpacity(0.6),
            thickness: 3,
          )
          // Divider()
        ],
      );
    }).toList();
    dev.log(
        "Renderizado de $runtimeType => construcción del ListView basandose en el elemento construido.");
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (context, i) => list[i],
    );
  }
}
