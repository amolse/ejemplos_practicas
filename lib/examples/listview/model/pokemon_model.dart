// ignore_for_file: unnecessary_getters_setters

/// Clase de un pokemon
class Pokemon {
  //Atributos de la clase solicitada (modificadas con late xq deben ser inicializadas)
  late String _name;
  late int _number;
  late PokemonType _type;
  late String _iconPath;

  static final List<Pokemon> initial = [
    Pokemon(
        name: "Bulbasaur",
        number: 1,
        type: PokemonType.plant,
        iconPath: "assets/examples/listview/img/bulbasaur.svg"),
    Pokemon(
        name: "Ivysaur",
        number: 2,
        type: PokemonType.plant,
        iconPath: "assets/examples/listview/img/bulbasaur.svg"),
    Pokemon(
        name: "Venusaur",
        number: 3,
        type: PokemonType.plant,
        iconPath: "assets/examples/listview/img/bulbasaur.svg"),
    Pokemon(
        name: "Charmander",
        number: 4,
        type: PokemonType.fire,
        iconPath: "assets/examples/listview/img/charmander.svg"),
    Pokemon(
        name: "Charmeleon",
        number: 5,
        type: PokemonType.fire,
        iconPath: "assets/examples/listview/img/charmander.svg"),
    Pokemon(
        name: "Charizard",
        number: 6,
        type: PokemonType.fire,
        iconPath: "assets/examples/listview/img/charmander.svg"),
    Pokemon(
        name: "Squirtle",
        number: 7,
        type: PokemonType.water,
        iconPath: "assets/examples/listview/img/squirtle.svg"),
    Pokemon(
        name: "Wartortle",
        number: 8,
        type: PokemonType.water,
        iconPath: "assets/examples/listview/img/squirtle.svg"),
    Pokemon(
        name: "Blastoise",
        number: 9,
        type: PokemonType.water,
        iconPath: "assets/examples/listview/img/squirtle.svg"),
  ];

  /// Materialización del contenido del JSON
  /// <String, dynamic> = <key, value>
  /// Nombre de la Propiedad y el valor de esta, como son distintas propiedades el tipo del valor puede variar, por eso está "tipado" como dynamic
  Pokemon.fromJson(Map<String, dynamic> jsonData, int id) {
    if (jsonData.isEmpty) return;

    _name = jsonData['name'];
    _number = id;
    _type = parseToPokemonType(jsonData['type']);
    _iconPath = "assets/examples/listview/img/${jsonData['iconPath']}";
  }

  /// Constructor con parámetros no nulables (required)
  Pokemon(
      {required String name,
      required int number,
      required PokemonType type,
      required String iconPath}) {
    _name = name;
    _number = number;
    _type = type;
    _iconPath = iconPath;
  }

  PokemonType get type => _type;
  set type(PokemonType value) {
    _type = value;
  }

  int get number => _number;
  set number(int value) {
    _number = value;
  }

  String get name => _name;
  set name(String value) {
    _name = value;
  }

  String get iconPath => _iconPath;
  set iconPath(String value) {
    _iconPath = value;
  }

  static PokemonType parseToPokemonType(String value) {
    switch (value.toLowerCase()) {
      case "water":
        return PokemonType.water;
      case "plant":
        return PokemonType.plant;
      case "fire":
        return PokemonType.fire;
      default:
        throw FormatException(
            "No se admite $value como valor a pasar a PokemonType");
    }
  }

  static String parseToString(PokemonType value) {
    switch (value) {
      case PokemonType.water:
        return "Agua";
      case PokemonType.plant:
        return "Planta";
      case PokemonType.fire:
        return "Fuego";
      default:
        return "";
    }
  }
}

/// Crear un enum PokemonType con los valores plant, fire y water, fuera de la clase, en el mismo fichero
enum PokemonType { water, plant, fire }

class Pokemons {
  ///Propiedad con la lista de pokemons.
  late List<Pokemon> lista;

  ///Constructor con parámetro no nulable (required)
  Pokemons({required this.lista});

  /// Materialización del contenido del JSON
  /// <String, dynamic> = <key, value>
  /// Nombre de la Propiedad y el valor de esta, como son distintas propiedades el tipo del valor puede variar, por eso está "tipado" como dynamic
  Pokemons.fromJson(Map<String, dynamic> jsonData) {
    lista = <Pokemon>[];
    if (jsonData['pokemons'] != null) {
      var i = 0;
      jsonData['pokemons'].forEach((futurePokemon) {
        lista.add(Pokemon.fromJson(futurePokemon, ++i));
      });
    }
  }

  /// Devolvemos la lista convertida a un texto enmaquetado.
  String parseTo() {
    String text = "Listado de Pokemons:\n";
    for (var pkmn in lista) {
      text =
          '$text   - Con el Id ${pkmn.number} tenemos a "${pkmn._name}" que es un pokemon de tipo ${Pokemon.parseToString(pkmn.type)}\n';
    }
    return text;
  }
}
