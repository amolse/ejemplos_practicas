import 'package:flutter/material.dart';
import 'dart:developer' as dev;

class MyList extends StatelessWidget {
  /// Container cuyo Child es un ListView de Text
  const MyList({super.key});

  @override
  Widget build(BuildContext context) {
    final items =
        List.generate(100, (i) => Text("Hola mundo, item $i")); //List.generate

    return Container(
      color: Colors.amber.withOpacity(0.4),
      child: ListView(
        padding: const EdgeInsets.all(20.0),
        children: items,
      ),
    );
  }
}

class MyListWhitDivider extends StatelessWidget {
  /// Container cuyo Child es uyn ListView de Columns
  const MyListWhitDivider({super.key});

  @override
  Widget build(BuildContext context) {
    final items = List.generate(
        100,
        (i) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Hola mundo, item $i"),
                const Divider(indent: 8, thickness: 2, color: Colors.blue)
              ],
            )); //List.generate

    return Container(
      color: Colors.amber.withOpacity(0.4),
      child: ListView(
        padding: const EdgeInsets.all(20.0),
        children: items,
      ),
    );
  }
}

class MyListTitle extends StatelessWidget {
  /// Container cuyo Child es un ListView de ListTitle
  const MyListTitle({super.key});

  @override
  Widget build(BuildContext context) {
    final items = List.generate(
      100,
      (i) => ListTile(
        title: Text("Hola mundo, item $i"),
        leading: const Icon(Icons.people),
        trailing: Container(height: 20, width: 20, color: Colors.purple),
        onTap: () => dev.log("Presionado sobre el elemento $i"),
      ),
    );

    return Container(
      color: Colors.amber.withOpacity(0.4),
      child: ListView(
        padding: const EdgeInsets.all(20.0),
        children: items,
      ),
    );
  }
}

class MyListLazyLoading extends StatelessWidget {
  /// Container cuyo Child es un ListView.builder de ListTitle
  const MyListLazyLoading({super.key});

  @override
  Widget build(BuildContext context) {
    final items = List.generate(
      100,
      (i) => ListTile(
        title: Text("Hola mundo, item $i"),
        leading: const Icon(Icons.people),
        trailing: Container(height: 20, width: 20, color: Colors.purple),
        onTap: () => dev.log("Presionado sobre el elemento $i"),
      ),
    );

    return Container(
      color: Colors.amber.withOpacity(0.4),
      child: ListView.builder(
        padding: const EdgeInsets.all(20.0),
        itemBuilder: (_, index) => items[index],
      ),
    );
  }
}

class MyListSeparated extends StatelessWidget {
  /// Container cuyo Child es un ListView.builder de ListTitle
  const MyListSeparated({super.key});

  @override
  Widget build(BuildContext context) {
    final items = List.generate(
      100,
      (i) => ListTile(
        title: Text("Hola mundo, item $i"),
        leading: const Icon(Icons.people),
        trailing: Container(height: 20, width: 20, color: Colors.purple),
        onTap: () => dev.log("Presionado sobre el elemento $i"),
      ),
    );

    return Container(
      color: Colors.amber.withOpacity(0.4),
      child: ListView.separated(
        padding: const EdgeInsets.all(20.0),
        itemBuilder: (_, index) => items[index],
        itemCount: items.length,
        separatorBuilder: (_, index) => const Divider(
          thickness: 2,
          color: Colors.black,
        ),
      ),
    );
  }
}
