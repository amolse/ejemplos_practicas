import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert'; // para poder usar la función jsonDecode
import 'package:ipc/examples/listview/model/pokemon_model.dart';

///En un widget, devolveremos un FutureBuilder, que hará de placeholder hasta que se reciba la información
class MyFutureAfter5Seconds extends StatelessWidget {
  ///Ejemplo progreso indeterminado
  const MyFutureAfter5Seconds({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FutureBuilder(
        builder: // que se va a mostrar en patalla
            //Este método se ejecutará en 2 ocasiones al inicio y cuando se reciba la información (finalice el Future)
            //Los datos los tendremos disponibles en el objeto snapshot.
            (context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            //En el momento que obtengamos la información, la mostraremos
            return Text("¡¡¡¡Hola, ${snapshot.data}!!!!");
          } else {
            //Si no tenemos la información, mostramos un indicador de progreso
            return const CircularProgressIndicator();
          }
        },
        future:
            //de dónde obtenemos la información, la función que obtiene los datos
            MyFutureAfter5Seconds.helloDelayed("Goku"),
      ),
    );
  }

  /// Definimos esta función que retorna, empaquetado en una Future, una String tras 5 segundos.
  /// En Flutter, la programación asíncrona se suele realizar con este tipo de objetos, los Future.
  /// Un Future representa una información que estará disponible en algún momento “en el futuro”.
  static Future<String> helloDelayed(String name) =>
      Future.delayed(const Duration(seconds: 5), () => "Sr $name");
}

class MyFutureHttpRequest extends StatelessWidget {
  ///Ejemplo progreso indeterminado
  const MyFutureHttpRequest({super.key});

  @override
  Widget build(BuildContext context) {
    var url = "https://api.npoint.io/56c4fc7a8ef7d90d5431";
    return Center(
      child: FutureBuilder(
        builder: // que se va a mostrar en patalla
            //Este método se ejecutará en 2 ocasiones al inicio y cuando se reciba la información (finalice el Future)
            //Los datos los tendremos disponibles en el objeto snapshot.
            (context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            //En el momento que obtengamos la información, la mostraremos
            return Text('Respuesta de la url "$url": \n\n ${snapshot.data}');
          } else {
            //Si no tenemos la información, mostramos un indicador de progreso
            return const LinearProgressIndicator();
          }
        },
        future:
            //de dónde obtenemos la información, la función que obtiene los datos
            MyFutureHttpRequest.getWebData(url),
      ),
    );
  }

  static Future<String> getWebData(String url) async {
    var result = await http.get(Uri.parse(url));
    return result.body;
  }
}

class MyFutureLoadJson extends StatelessWidget {
  ///Ejemplo progreso indeterminado
  const MyFutureLoadJson({super.key});

  @override
  Widget build(BuildContext context) {
    //recuerda: debe estar el asset indicado en pubspec.yaml / flutter / assets
    var url = "assets/examples/listview/data/pokemons.json";
    return Center(
      child: FutureBuilder(
        builder: // que se va a mostrar en patalla
            //Este método se ejecutará en 2 ocasiones al inicio y cuando se reciba la información (finalice el Future)
            //Los datos los tendremos disponibles en el objeto snapshot.
            (context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            //En el momento que obtengamos la información, la mostraremos
            // Como datos tenemos un string, tenemos que pasarlo a un formato json para tratarlo como tal.
            var data = jsonDecode(snapshot.data!);
            var pokemons = Pokemons.fromJson(data);
            return Text(
                'Tratamiento del archivo "$url": \n\n ${pokemons.parseTo()}');
          } else {
            if (snapshot.hasError) {
              return Text(
                  'Error en el tratamiento del archivo "$url": \n\n ${snapshot.error.toString()}');
            }
            //Si no tenemos la información, mostramos un indicador de progreso
            return const LinearProgressIndicator();
          }
        },
        future:
            //de dónde obtenemos la información, la función que obtiene los datos
            MyFutureLoadJson.loadJson(context, url),
      ),
    );
  }

  static Future<String> loadJson(BuildContext context, String url,
      {bool withDelay = true}) async {
    //Retraso la lectura 5 segundos para ver el loader
    if (withDelay) {
      return Future.delayed(const Duration(seconds: 5),
          () => DefaultAssetBundle.of(context).loadString(url));
    }
    //Lo leemos directamente sin esperar
    return DefaultAssetBundle.of(context).loadString(url);
  }
}
