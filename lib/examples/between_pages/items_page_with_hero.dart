import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'details_page_with_hero.dart';

class ItemsPageWithHero extends StatelessWidget {
  final String title;

  const ItemsPageWithHero({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Items"),
      ),
      body: _Items(),
    );
  }
}

class _Items extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      // color: Colors.blue,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            child: Hero(
                tag: "dog",
                child: Image.asset(
                  "assets/examples/between_pages/img/dog.gif",
                  height: 125,
                )),
            onTap: () {
              var route = MaterialPageRoute(
                  builder: (_) => const DetailsPageWithHero(
                      imgPath: "assets/examples/between_pages/img/dog.gif",
                      heroTag: "dog"));
              Navigator.of(context).push(route);
            },
          ),
          GestureDetector(
            child: Hero(
                tag: "cat",
                child: Image.asset(
                  "assets/examples/between_pages/img/cat.gif",
                  height: 125,
                )),
            onTap: () {
              var route = MaterialPageRoute(
                  builder: (_) => const DetailsPageWithHero(
                      imgPath: "assets/examples/between_pages/img/cat.gif",
                      heroTag: "cat"));
              Navigator.of(context).push(route);
            },
          ),
          GestureDetector(
            child: Hero(
                tag: "agg",
                child: Image.asset(
                  "assets/examples/between_pages/img/agg.gif",
                  height: 125,
                )),
            onTap: () {
              var route = CupertinoPageRoute(
                  builder: (_) => const DetailsPageWithHero(
                      imgPath: "assets/examples/between_pages/img/agg.gif",
                      heroTag: "agg"));
              Navigator.of(context).push(route);
            },
          ),
        ],
      ),
    );
  }
}
