import 'package:flutter/material.dart';

class DetailsPageWithHero extends StatelessWidget {
  final String imgPath;
  final String heroTag;

  const DetailsPageWithHero(
      {super.key, required this.imgPath, required this.heroTag});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Details "),
        ),
        body: Center(
            child: Hero(
          tag: heroTag,
          child: SizedBox(
            height: 200,
            width: double.infinity,
            child: Image.asset(imgPath),
          ),
        )));
  }
}
