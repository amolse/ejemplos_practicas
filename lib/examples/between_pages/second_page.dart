import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

class SecondPage extends StatelessWidget {
  final Tuple2<String, Color> data;

  const SecondPage({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Second page"),
      ),
      body: SizedBox(
        height: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              data.item1,
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              width: 30,
            ),
            Icon(
              Icons.color_lens,
              color: data.item2,
              size: 40,
            )
          ],
        ),
      ),
    );
  }
}
