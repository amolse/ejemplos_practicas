import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

import 'second_page.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("First page"),
      ),
      body: SizedBox(
        height: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => const SecondPage(
                          data: Tuple2("Rojo", Colors.red),
                        )));
              },
              style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
              child: const Text("Red"),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => const SecondPage(
                          data: Tuple2("Verde", Colors.green),
                        )));
              },
              style: ElevatedButton.styleFrom(backgroundColor: Colors.green),
              child: const Text("Green"),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => const SecondPage(
                          data: Tuple2("Azul", Colors.blue),
                        )));
              },
              style: ElevatedButton.styleFrom(backgroundColor: Colors.blue),
              child: const Text("Blue"),
            ),
          ],
        ),
      ),
    );
  }
}
